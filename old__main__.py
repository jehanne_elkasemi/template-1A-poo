from ptd.importers.importer_json_gz import ImporterJsonGz
from ptd.transformation.moyenne_glissante import MoyenneGlissante
from ptd.transformation.selectionnervar import SelectionnerVar
from ptd.transformation.standardiser import Standardiser
from ptd.transformation.centrer import Centrer
from ptd.transformation.joindre import Joindre
from ptd.transformation.plot import Plot
from ptd.modeles.table import Table
from ptd.modeles.pipeline import Pipeline
from ptd.importers.importer_csv import ImporterCsv
from ptd.exporters.exporter_csv import ExporterCsv
from ptd.importers.importer_csv_gz import ImporterCsvGz
from ptd.modeles.bonus import obtention_region
import os

if __name__ == "__main__":
    #table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
    #var_standardisee = Standardiser("nb_habitant")
    #print(var_standardisee.appliquer(table))
    #var_centre = Centrer("nb_habitant")
    #print(var_centre.appliquer(table))
    #table2 = Table(["id", "superficie"], [["ARA", 69711], ["BFC", 47783]])
    #joindre = Joindre(table, "id")
    #print(joindre.appliquer(table2))
    #table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
    #data = ImporterCsv('P:/projet_ipoo/donnee/postesSynopAvecRegions.csv', ';')
    #test = data.importer()
    #test.asNumeric("Altitude")
    #print(test)
    #operateur = Pipeline([SelectionnerVar(["ID", "Region"])])
    #print(operateur.run(test))
    #salaire = Table(['Name', 'Profession', 'Salary'],[['Alice', np.nan, 122000], ['Bob', 'Engineer', 77000.4], ['Ann', 'Manager', 119000.5]])
    #ExporterCsv('C:/Users/nunes/Desktop/projet_traitement_de_donnees/', 'salaire', ',').exporter(salaire)
    #table_de_test=ImporterCsv('/home/briel/Documents/ensai1A/projet_trait_données/postesSynop(1).csv',['Latitude', 'Longitude', 'Altitude'],'mq').importer()
    #print(table_de_test)
    #print(table_de_test)
    #centre=Centrer('Altitude')
    #table_de_test2=centre.appliquer(table_de_test)
    #print(table_de_test2)
    #plot=Plot('Latitude','Longitude', 'long en fct de la')
    #print(table_de_test)
    #plot.appliquer(table_de_test)

    # IMPORTATION
    #importer des tables de données météo ou consommation énergétiques, mettre les chemin de l'utilisateur, les tables sont dans data
    table_meteo_csv_gz=ImporterCsvGz('/home/briel/Téléchargements/Fichiers de Données .csv.gz-20220529/données_météo/synop.201301.csv.gz',[],'mq',';').importer()
    print(table_meteo_csv_gz)
    table_energie_json=ImporterJsonGz('/home/briel/Téléchargements/Fichiers de Données .json.gz-20220529/données_électricité/2013-01.json.gz',[],'na').importer()
    #print(table_energie_json)

    ## TRANSFORMATIONS
    # Calcul de moyenne glissante de salaire versés à des dates différentes
    # la fenêtre est d'un rayon de 2 jours (ie les moyennes sont calculées sur des fenêtres de 2*2 + 1 = 5 jours)
    salaire = Table(['Name', 'Date', 'Salary', 'Date_format_datetime'], [['Alice', '21/04/2021', 122000, datetime.datetime(2021, 4, 21, 0, 0)], ['Bob', '22/04/2021', 77000.4, datetime.datetime(2021, 4, 22, 0, 0)], ['Anne', '24/04/2021', 119000.5, datetime.datetime(2021, 4, 24, 0, 0)], ['Steve', '21/04/2021', 12000, datetime.datetime(2021, 4, 21, 0, 0)], ['Gabrielle', '18/04/2021', 68000, datetime.datetime(2021, 4, 18, 0, 0)], ['Jehanne', '25/04/2021', 229000, datetime.datetime(2021, 4, 25, 0, 0)], ['Donatien', '19/04/2021', np.nan, datetime.datetime(2021, 4, 19, 0, 0)]])
    moyenne_glissante = MoyenneGlissante(variable = 'Salary', voisinage = "2j", strptime = "%dj")
    salaire_glissant = moyenne_glissante.appliquer(salaire)
    print(salaire_glissant)
    
    ## EXPORTATION
    # Exportation
    salaire_export = Table(['Name', 'Profession', 'Salary'],[['Alice', 'Data Scientist', 122000], ['Bob', 'Engineer', 77000.4], ['Ann', 'Manager', 119000.5]])
    # Remplacer le chemin 'P:/projet_de_traitement_de_donnees/' par le chemin désiré
    ExporterCsv('P:/projet_de_traitement_de_donnees/', 'salaire', ',').exporter(salaire_export)
    
    #bonus
    #importer=ImporterCsv('/home/briel/Documents/ensai1A/projet_trait_données/postesSynop(1).csv',['Latitude', 'Longitude', 'Altitude'])
    #table_bonus=importer.importer()
    #obtention_region(table_bonus, 'Latitude', 'Longitude')


    






