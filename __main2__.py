from ptd.transformation.selectionnervar import SelectionnerVar
import numpy as np
from ptd.importers.importer_csv import ImporterCsv
from ptd.importers.importer_csv_gz import ImporterCsvGz
from ptd.importers.importer_json_gz import ImporterJsonGz
from ptd.exporters.exporter_csv import ExporterCsv
from ptd.transformation.joindre import Joindre
from ptd.transformation.plot import Plot
from ptd.transformation.standardiser import Standardiser
from ptd.transformation.centrer import Centrer
from ptd.modeles.table import Table
from ptd.modeles.pipeline import Pipeline
from ptd.transformation.formater_date import FormaterDate
from ptd.transformation.moyenne_glissante import MoyenneGlissante
import ptd.modeles.bonus as b
import ptd.modeles.boite_a_outil as fct
import os

if __name__ == "__main__":
    
    #Y a-t-il un lien entre la consommation électrique et la température en 2013 entre janvier et juillet?
    """conso_2013 = ImporterJsonGz(str(os.getcwd()) +"\\data\\Fichiers de Données .json.gz-20220529\\données_électricité\\2013-01.json.gz",[], nom_na='N/A').importer()
    meteo_2013 = ImporterCsvGz(str(os.getcwd()) +"\\data\\Fichiers de Données .csv.gz-20220529\\données_météo\\synop.201301.csv.gz",['dd', 'ff', 't', 'td', 'u', 'vv', 'ww', 'w1', 'w2', 'n', 'nbas', 'hbas', 'cl', 'cm', 'ch', 'pres', 'niv_bar', 'geop', 'tend24', 'tn12', 'tn24', 'tx12', 'tx24', 'tminsol', 'sw', 'tw', 'raf10', 'rafper', 'per', 'etat_sol', 'ht_neige', 'ssfrai', 'perssfrai', 'rr1', 'rr3', 'rr6', 'rr12', 'rr24', 'phenspe1', 'phenspe2', 'phenspe3', 'phenspe4', 'nnuage1', 'ctype1', 'hnuage1', 'nnuage2', 'ctype2', 'hnuage2', 'nnuage3', 'ctype3', 'hnuage3', 'nnuage4', 'ctype4', 'hnuage4'] ,'mq', ";").importer()
    for k in range(1,3):
        if k<10:
            importe_conso = ImporterJsonGz(str(os.getcwd()) +"\\data\\Fichiers de Données .json.gz-20220529\\données_électricité\\2013-0"+ str(k) + ".json.gz",[], nom_na='N/A')
            importe_meteo = ImporterCsvGz(str(os.getcwd()) +"\\data\\Fichiers de Données .csv.gz-20220529\\données_météo\\synop.20130" + str(k) + ".csv.gz",['dd', 'ff', 't', 'td', 'u', 'vv', 'ww', 'w1', 'w2', 'n', 'nbas', 'hbas', 'cl', 'cm', 'ch', 'pres', 'niv_bar', 'geop', 'tend24', 'tn12', 'tn24', 'tx12', 'tx24', 'tminsol', 'sw', 'tw', 'raf10', 'rafper', 'per', 'etat_sol', 'ht_neige', 'ssfrai', 'perssfrai', 'rr1', 'rr3', 'rr6', 'rr12', 'rr24', 'phenspe1', 'phenspe2', 'phenspe3', 'phenspe4', 'nnuage1', 'ctype1', 'hnuage1', 'nnuage2', 'ctype2', 'hnuage2', 'nnuage3', 'ctype3', 'hnuage3', 'nnuage4', 'ctype4', 'hnuage4'] , 'mq', ";")
        else:
            importe_conso = ImporterJsonGz(str(os.getcwd()) +"\\data\\Fichiers de Données .json.gz-20220529\\données_électricité\\2013-"+ str(k) + ".json.gz",[], nom_na='N/A')
            importe_meteo = ImporterCsvGz(str(os.getcwd()) +"\\data\\Fichiers de Données .csv.gz-20220529\\données_météo\\synop.2013" + str(k) + ".csv.gz",['dd', 'ff', 't', 'td', 'u', 'vv', 'ww', 'w1', 'w2', 'n', 'nbas', 'hbas', 'cl', 'cm', 'ch', 'pres', 'niv_bar', 'geop', 'tend24', 'tn12', 'tn24', 'tx12', 'tx24', 'tminsol', 'sw', 'tw', 'raf10', 'rafper', 'per', 'etat_sol', 'ht_neige', 'ssfrai', 'perssfrai', 'rr1', 'rr3', 'rr6', 'rr12', 'rr24', 'phenspe1', 'phenspe2', 'phenspe3', 'phenspe4', 'nnuage1', 'ctype1', 'hnuage1', 'nnuage2', 'ctype2', 'hnuage2', 'nnuage3', 'ctype3', 'hnuage3', 'nnuage4', 'ctype4', 'hnuage4'] ,'mq', ";")
        conso = importe_conso.importer()
        meteo = importe_meteo.importer()
        for k in conso.body:
            conso_2013.ajouter_ligne(k)
        for k in meteo.body:
            meteo_2013.ajouter_ligne(k)
    table_de_correspondance = ImporterCsv(str(os.getcwd()) +"\\data\\Fichiers de Données .csv.gz-20220529\\postesSynopAvecRegions.csv", []).importer()
    #table_de_correspondance.ajouter_colonne("region", b.obtention_region(table_de_correspondance, 'Latitude', 'Altitude'))
    table_meteo_region = Joindre(table_de_correspondance, "ID", "numer_sta").appliquer(meteo_2013)
    table_meteo_region = FormaterDate('date', '%Y%m%d%H%M%S').appliquer(table_meteo_region)
    for k in conso_2013.body:
       k[7]= k[7][:19]
    conso_2013.body=conso.body[1:]
    table_conso_date = FormaterDate('date_heure', '%Y-%m-%dT%H:%M:%S').appliquer(conso_2013)
    #table_conso_electrique = SelectionnerVar(['Date_format_datetime', )
    table_join=Joindre(table_conso_date, 'region', 'Region', 'Date_format_datetime').appliquer(table_meteo_region)
    export = ExporterCsv(str(os.getcwd()) + "\\data\\resultat\\" , "table_join_2013", ";")
    export.exporter(table_join)"""

    importe_meteo = ImporterCsv(str(os.getcwd()) +"\\data\\resultat\\table_join_2013.csv",['consommation_brute_electricite_rte','dd', 'ff', 't', 'td', 'u', 'vv', 'ww', 'w1', 'w2', 'n', 'nbas', 'hbas', 'cl', 'cm', 'ch', 'pres', 'niv_bar', 'geop', 'tend24', 'tn12', 'tn24', 'tx12', 'tx24', 'tminsol', 'sw', 'tw', 'raf10', 'rafper', 'per', 'etat_sol', 'ht_neige', 'ssfrai', 'perssfrai', 'rr1', 'rr3', 'rr6', 'rr12', 'rr24', 'phenspe1', 'phenspe2', 'phenspe3', 'phenspe4', 'nnuage1', 'ctype1', 'hnuage1', 'nnuage2', 'ctype2', 'hnuage2', 'nnuage3', 'ctype3', 'hnuage3', 'nnuage4', 'ctype4', 'hnuage4'] , 'N/A', ";")
    table_debut = importe_meteo.importer()
    #print(table_debut.header)
    table2 = SelectionnerVar(['region', 't', 'consommation_brute_electricite_rte', 'Date_format_datetime']).appliquer(table_debut)
    table = fct.enlever_val_manq(table2)
    #print(table)
    pipeline = Pipeline([SelectionnerVar(['region', 't', 'consommation_brute_electricite_rte', 'Date_format_datetime']), Plot('t', 'consommation_brute_electricite_rte')])
    table_finale = pipeline.run(table_debut)
    #print(table_finale.header)
    #print(table_finale.body[0])
    #print(table_finale.body[1])




    
    
 

    
    


        
    
#salaire = Table(['Name', 'Profession', 'Salary'],[['Alice', np.nan, 122000], ['Bob', 'Engineer', 77000.4], ['Ann', 'Manager', 119000.5]])
#ExporterCsv('C:/Users/nunes/Desktop/projet_traitement_de_donnees/', 'salaire', ',').exporter(salaire)
    






