from ptd.importers.importer import Importer
from ptd.modeles.table import Table
import gzip
import json
from ptd.transformation.remplacer_val_manquante import RemplacerValManquante
import copy
import numpy as np

class ImporterJson(Importer):
    '''Créer un objet permettant d'importer un fichier json sous la forme d'un objet de type Table

        Paramètres
        ----------
        chemin : str (chemin du fichier que l'on souhaite importer)
        nom_var_num : str list (liste des nom de var en numerique)
        nom_na : str (str qui represente les valeurs manquantes)
        nom_var_dico : str list (liste des noms de var qui sont elles meme des dictionnaire dans le fichier json)

        Retourne
        -------
        None

        Exemples
        --------
        >>>

        '''
    def __init__(self, chemin,nom_var_num=[], nom_na='N/A',nom_var_dico=[]):
        super().__init__(chemin,nom_var_num,nom_na)
        self.nom_var_dico=nom_var_dico

    def importer(self):
        '''Permet d'importer sous la forme d'un objet de type Table le fichier 

        Paramètres
        ----------

        Retourne
        -------
        Table
        la table correspondant au fichier dont on a donné le chemin

        Exemples
        --------

        '''
        with gzip.open(self.chemin, mode='rt', encoding="utf-8") as gzfile :
            dic = json.load(gzfile)


        list_variable = list(map(list, [dic[k]['fields'].keys() for k in range(len(dic))]))
        header = np.unique([item for sublist in list_variable for item in sublist])
        self.header = header.tolist()
        body = [[col_name for col_name in header] ]
            
        for line in dic:
            list_value = []
            for col_name in header:
                try:
                    list_value.append(line["fields"][col_name])
                except KeyError:
                    list_value.append(self.nom_na)
            body.append(list_value)
        table1 = Table(header, body)
        
        L=[]
        for k in table1.header:
            L.append(k)
            table1.header = L

        for nom in self.nom_var_num : 
            table1.as_numeric(nom)
        remplacer=RemplacerValManquante(self.nom_na)
        table2=remplacer.appliquer(table1)
        return table2

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    
    
    