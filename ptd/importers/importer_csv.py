from ptd.importers.importer import Importer
from ptd.modeles.table import Table
import csv
from ptd.transformation.remplacer_val_manquante import RemplacerValManquante

class ImporterCsv(Importer):
    '''Créer un objet permettant d'importer un fichier csv sous la forme d'un objet de type Table

    Paramètres
    ----------
    chemin : str (chemin du fichier que l'on souhaite importer)
    nom_var_num : str list (liste des nom de var en numerique)
    nom_na : str (str qui represente les valeurs manquantes)
    separateur : str (caractère qui sert de séparateur dans le fichier)

    Retourne
    -------
    None

    Exemples
    --------
    >>>

    '''
    def __init__(self, chemin, nom_var_num, nom_na='N/A', separateur=';'):
        super().__init__(chemin, nom_var_num, nom_na)
        self.separateur=separateur

    def importer(self):
        '''Permet d'importer sous la forme d'un objet de type Table le fichier 

        Paramètres
        ----------

        Retourne
        -------
        Table
        la table correspondant au fichier dont on a donné le chemin

        Exemples
        --------
        >>> ImporterCsv=ImporterCsv('/home/briel/Documents/ensai1A/projet_trait_données/postesSynop(1).csv',['Latitude', 'Longitude', 'Altitude'])
        >>> table=ImporterCsv.importer()
        >>> print(table)
        ['ID', 'Nom', 'Latitude', 'Longitude', 'Altitude']
        **************************************************
        ['07005', 'ABBEVILLE', 50.136, 1.834, 69.0]
        ['07015', 'LILLE-LESQUIN', 50.57, 3.0975, 47.0]
        ['07020', 'PTE DE LA HAGUE', 49.725167, -1.939833, 6.0]
        ['07027', 'CAEN-CARPIQUET', 49.18, -0.456167, 67.0]
        ['07037', 'ROUEN-BOOS', 49.383, 1.181667, 151.0]
        ['07072', 'REIMS-PRUNAY', 49.209667, 4.155333, 95.0]
        ['07110', 'BREST-GUIPAVAS', 48.444167, -4.412, 94.0]
        ['07117', "PLOUMANAC'H", 48.825833, -3.473167, 55.0]
        ['07130', 'RENNES-ST JACQUES', 48.068833, -1.734, 36.0]
        ['07139', 'ALENCON', 48.4455, 0.110167, 143.0]
        ['07149', 'ORLY', 48.716833, 2.384333, 89.0]
        ['07168', 'TROYES-BARBEREY', 48.324667, 4.02, 112.0]
        ['07181', 'NANCY-OCHEY', 48.581, 5.959833, 336.0]
        ['07190', 'STRASBOURG-ENTZHEIM', 48.5495, 7.640333, 150.0]
        ['07207', 'BELLE ILE-LE TALUT', 47.294333, -3.218333, 34.0]
        ['07222', 'NANTES-BOUGUENAIS', 47.15, -1.608833, 26.0]
        ['07240', 'TOURS', 47.4445, 0.727333, 108.0]
        ['07255', 'BOURGES', 47.059167, 2.359833, 161.0]
        ['07280', 'DIJON-LONGVIC', 47.267833, 5.088333, 219.0]
        ['07299', 'BALE-MULHOUSE', 47.614333, 7.51, 263.0]
        ['07314', 'PTE DE CHASSIRON', 46.046833, -1.4115, 11.0]
        ['07335', 'POITIERS-BIARD', 46.593833, 0.314333, 123.0]
        ['07434', 'LIMOGES-BELLEGARDE', 45.861167, 1.175, 402.0]
        ['07460', 'CLERMONT-FD', 45.786833, 3.149333, 331.0]
        ['07471', 'LE PUY-LOUDES', 45.0745, 3.764, 833.0]
        ['07481', 'LYON-ST EXUPERY', 45.7265, 5.077833, 235.0]
        ['07510', 'BORDEAUX-MERIGNAC', 44.830667, -0.691333, 47.0]
        ['07535', 'GOURDON', 44.745, 1.396667, 260.0]
        ['07558', 'MILLAU', 44.1185, 3.0195, 712.0]
        ['07577', 'MONTELIMAR', 44.581167, 4.733, 73.0]
        ['07591', 'EMBRUN', 44.565667, 6.502333, 871.0]
        ['07607', 'MONT-DE-MARSAN', 43.909833, -0.500167, 59.0]
        ['07621', 'TARBES-OSSUN', 43.188, 0.0, 360.0]
        ['07627', 'ST GIRONS', 43.005333, 1.106833, 414.0]
        ['07630', 'TOULOUSE-BLAGNAC', 43.621, 1.378833, 151.0]
        ['07643', 'MONTPELLIER', 43.577, 3.963167, 2.0]
        ['07650', 'MARIGNANE', 43.437667, 5.216, 9.0]
        ['07661', 'CAP CEPET', 43.079333, 5.940833, 115.0]
        ['07690', 'NICE', 43.648833, 7.209, 2.0]
        ['07747', 'PERPIGNAN', 42.737167, 2.872833, 42.0]
        ['07761', 'AJACCIO', 41.918, 8.792667, 5.0]
        ['07790', 'BASTIA', 42.540667, 9.485167, 10.0]
        ['61968', 'GLORIEUSES', -11.582667, 47.289667, 3.0]
        ['61970', 'JUAN DE NOVA', -17.054667, 42.712, 9.0]
        ['61972', 'EUROPA', -22.344167, 40.340667, 6.0]
        ['61976', 'TROMELIN', -15.887667, 54.520667, 7.0]
        ['61980', 'GILLOT-AEROPORT', -20.8925, 55.528667, 8.0]
        ['61996', 'NOUVELLE AMSTERDAM', -37.795167, 77.569167, 27.0]
        ['61997', 'CROZET', -46.4325, 51.856667, 146.0]
        ['61998', 'KERGUELEN', -49.352333, 70.243333, 29.0]
        ['67005', 'PAMANDZI', -12.8055, 45.282833, 7.0]
        ['71805', 'ST-PIERRE', 46.766333, -56.179167, 21.0]
        ['78890', 'LA DESIRADE METEO', 16.335, -61.004, 27.0]
        ['78894', 'ST-BARTHELEMY METEO', 17.9015, -62.852167, 44.0]
        ['78897', 'LE RAIZET AERO', 16.264, -61.516333, 11.0]
        ['78922', 'TRINITE-CARAVEL', 14.7745, -60.875333, 26.0]
        ['78925', 'LAMENTIN-AERO', 14.595333, -60.995667, 3.0]
        ['81401', 'SAINT LAURENT', 5.4855, -54.031667, 5.0]
        ['81405', 'CAYENNE-MATOURY', 4.822333, -52.365333, 4.0]
        ['81408', 'SAINT GEORGES', 3.890667, -51.804667, 6.0]
        ['81415', 'MARIPASOULA', 3.640167, -54.028333, 106.0]
        ['89642', "DUMONT D'URVILLE", -66.663167, 140.001, 43.0]
        '''
        data = []
        with open(self.chemin, encoding="utf-8") as csv_file:
            synopreader = csv.reader(csv_file, delimiter=self.separateur)
            for row in synopreader :
                data.append(row)
        table=Table(data[0], data[1:])
        remplacer=RemplacerValManquante(self.nom_na)
        table2=remplacer.appliquer(table)
        for nom in self.nom_var_num : 
            table2.as_numeric(nom)
        return table2


if __name__ == '__main__':
    import doctest
    doctest.testmod()