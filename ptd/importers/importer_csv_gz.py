from ptd.importers.importer import Importer
from ptd.modeles.table import Table
import gzip
import csv
from ptd.transformation.remplacer_val_manquante import RemplacerValManquante
from ptd.transformation.remplacer_val_manquante import RemplacerValManquante

class ImporterCsvGz(Importer):
    '''Créer un objet permettant d'importer un fichier csv.gz sous la forme d'un objet de type Table

    Paramètres
    ----------
    chemin : str (chemin du fichier que l'on souhaite importer)
    separateur : str (caractère qui sert de séparateur dans le fichier)
    nom_var_num : str list (liste des noms de var en numérique)
    nom_na : str (chaine de caractere qui represente une valeur manquante)

    Retourne
    -------
    None

    Exemples
    --------
    >>>

    '''
    def __init__(self, chemin, nom_var_num=[], nom_na='N/A', separateur=';'):
        self.separateur=separateur
        self.chemin =chemin
        self.nom_var_num = nom_var_num
        self.nom_na =nom_na
        self.separateur = separateur

    def importer(self):
        '''Permet d'importer sous la forme d'un objet de type Table le fichier 

        Paramètres
        ----------

        Retourne
        -------
        Table
        la table correspondant au fichier dont on a donné le chemin

        Exemples
        --------
        >>>
        '''

        data = []
        with gzip.open(self.chemin, mode='rt', encoding="utf-8") as gzfile :
            synopreader = csv.reader(gzfile, delimiter=self.separateur)
            for row in synopreader :
                data.append(row)
        body=[]
        #for individu in data[1:]:
        #    body.append(individu[0:-1])
        #    print(individu[0:-1])
        table=Table(data[0], data[1:])
        remplacer=RemplacerValManquante(self.nom_na)
        table2=remplacer.appliquer(table)
        for nom in self.nom_var_num : 
            table.as_numeric(nom)
        return table2