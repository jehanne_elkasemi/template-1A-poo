from abc import ABC, abstractmethod

class Importer(ABC):
    def __init__(self, chemin, nom_var_num = [], nom_na = 'N/A'):
        self.chemin=chemin
        self.nom_var_num=nom_var_num
        self.nom_na=nom_na
    @abstractmethod
    def importer(self):
        pass
