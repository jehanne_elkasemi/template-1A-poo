from ptd.importers.importer import Importer
from ptd.modeles.table import Table
import gzip
import json
from ptd.transformation.remplacer_val_manquante import RemplacerValManquante
import copy
import numpy as np

class ImporterJsonGz2(Importer):
    '''Créer un objet permettant d'importer un fichier json sous la forme d'un objet de type Table

        Paramètres
        ----------
        chemin : str (chemin du fichier que l'on souhaite importer)
        nom_var_num : str list (liste des nom de var en numerique)
        nom_na : str (str qui represente les valeurs manquantes)
        nom_var_dico : str list (liste des noms de var qui sont elles meme des dictionnaire dans le fichier json)

        Retourne
        -------
        None

        Exemples
        --------
        >>>

        '''
    def __init__(self, chemin,nom_var_num=[], nom_na='N/A'):
        super().__init__(chemin,nom_var_num,nom_na)

    def importer(self):
        '''Permet d'importer sous la forme d'un objet de type Table le fichier 

        Paramètres
        ----------

        Retourne
        -------
        Table
        la table correspondant au fichier dont on a donné le chemin

        Exemples
        --------

        '''
        with gzip.open(self.chemin, mode='rt', encoding="utf-8") as gzfile :
            data = json.load(gzfile)
        header=[]
        for var in data[0].keys():
            header.append(var)
        body=[]
        for dico in data :
            individu=[]
            for cle in header :
                individu.append(dico[cle])
            body.append(individu)
        table=Table(header, body)
        remplacer=RemplacerValManquante(self.nom_na)
        table2=remplacer.appliquer(table)
        for nom in self.nom_var_num : 
            table2.as_numeric(nom)
        return table2

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    
    
    