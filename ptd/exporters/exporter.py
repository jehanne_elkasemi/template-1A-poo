from abc import ABC, abstractmethod


class Exporter(ABC):
    def __init__(self, dossier, fichier, nom_na):
        self.dossier = dossier
        self.fichier = fichier
        self.nom_na = nom_na

    @abstractmethod
    def exporter(self, table):
        pass

# supprimer la colonne id avant d'exporter
