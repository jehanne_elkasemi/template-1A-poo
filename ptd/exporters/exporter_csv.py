import csv
from ptd.exporters.exporter import Exporter
from ptd.modeles.table import Table
import numpy as np
from copy import deepcopy

class ExporterCsv(Exporter):
    def __init__(self, dossier, fichier, separateur = ';', nom_na = 'N/A'):
        '''Créer un objet permettant d'exporter toute table avec des options choisies en argument

        Paramètres
        ----------
        dossier : str (chemin du dossier)
        fichier : str (nom du fichier sans extension .csv)
        nom_na : str (chaîne de caractère renvoyant à valeur manquante)
        separateur : str

        Retourne
        -------
        (néant)

        Exemples
        --------
        >>> exporter_csv = ExporterCsv('P:/projet_de_traitement_de_donnees/', 'salaire', ',')
        
        '''
        super().__init__(dossier, fichier, nom_na)
        self.separateur = separateur

    def __str__(self):
        return "ExporterCsv({}/{}.csv, sep : {}, NA : {})".format(
            self.dossier, self.fichier, self.separateur, self.nom_na)

    def exporter(self, table):
        '''Exporte la table table au format CSV selon les options de la classe

        Paramètre
        ----------
        table : Table

        Retourne
        ----------
        (néant)

        Exemples
        ----------
        >>> salaire = Table(['Name', 'Profession', 'Salary'],[['Alice', 'Data Scientist', 122000], ['Bob', 'Engineer', 77000.4], ['Ann', 'Manager', 119000.5]])
        >>> ExporterCsv('P:/projet_de_traitement_de_donnees/', 'salaire', ',').exporter(salaire)
        
        '''        
        body = table.body
        # Convertissage des nan en valeurs manquantes
        # Convertissage de la table de type Table en liste de listes liste_table (ligne à ligne)
        liste_table = [table.header]
        for individu in body :
            individu_corrige = deepcopy(individu)
            for i in range(len(individu)):
                if isinstance(individu[i], float) and np.isnan(individu[i]):
                    individu_corrige[i]=self.nom_na
            liste_table.append(individu_corrige)
        # Écriture
        with open(self.dossier + self.fichier + '.csv', 'w', newline='', encoding='utf-8') as f:
            writer = csv.writer(f, delimiter=self.separateur)
            writer.writerows(liste_table)


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)

# salaire = Table(['Name', 'Profession', 'Salary'],[['Alice', np.nan, 122000], ['Bob', 'Engineer', 77000.4], ['Ann', 'Manager', 119000.5]])
# ExporterCsv(dossier = 'P:/projet_de_traitement_de_donnees/', fichier = 'salaire', separateur = ';').exporter(salaire)