# from transformation import Transformation
# import sys, os
# sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
# from modeles.table import Table
from ptd.transformation.transformation import Transformation
from ptd.modeles.table import Table
import numpy as np
import datetime
from copy import deepcopy

class MoyenneGlissante(Transformation) :
    """Crée une classe MoyenneGlissante
    Paramètres : 
    ------------
        variable (str): nom de la variable sur lequel appliquer la moyenne glissante
        date (str, optional): nom de la variable de date,
                par défaut est 'Date_format_datetime'.
        voisinage (str, optional): durée du voisinage équivalent au rayon de la fenêtre de moyenne glissante,
                par défaut est "1j" (1 jour).
        strptime (str, optional): format de conversion de la chaîne de caractères de voisinage en pour une conversion strptime,
                par défaut est "%dj".
    
    Retourne :
    ------------
        retourne une table de moyenne glissante dont le rayon de la fenêtre correspond à une durée
        
    Exemples :
    ------------
    >>> salaire = Table(['Name', 'Date', 'Salary', 'Date_format_datetime'], [['Alice', '21/04/2021', 122000, datetime.datetime(2021, 4, 21, 0, 0)], ['Bob', '22/04/2021', 77000.4, datetime.datetime(2021, 4, 22, 0, 0)], ['Anne', '24/04/2021', 119000.5, datetime.datetime(2021, 4, 24, 0, 0)], ['Steve', '21/04/2021', 12000, datetime.datetime(2021, 4, 21, 0, 0)], ['Gabrielle', '18/04/2021', 68000, datetime.datetime(2021, 4, 18, 0, 0)], ['Jehanne', '25/04/2021', 229000, datetime.datetime(2021, 4, 25, 0, 0)], ['Donatien', '19/04/2021', np.nan, datetime.datetime(2021, 4, 19, 0, 0)]])
    >>> moyenne_glissante = MoyenneGlissante(variable = 'Salary', voisinage = "2j", strptime = "%dj")
    >>> salaire_glissant = moyenne_glissante.appliquer(salaire)
    >>> print(salaire_glissant)
    ['Name', 'Date', 'Salary', 'Date_format_datetime']
    **************************************************
    ['Alice', '21/04/2021', 70333.46666666666, datetime.datetime(2021, 4, 21, 0, 0)]
    ['Bob', '22/04/2021', 69583.59166666666, datetime.datetime(2021, 4, 22, 0, 0)]
    ['Steve', '21/04/2021', 50639.01944444444, datetime.datetime(2021, 4, 21, 0, 0)]
    """
    def __init__(self, variable:str, date:str = 'Date_format_datetime', voisinage:str = "1j", strptime:str = "%dj"):
        """Crée une classe MoyenneGlissante

        Paramètres : 
        ------------
            variable (str): nom de la variable sur lequel appliquer la moyenne glissante
            date (str, optional): nom de la variable de date,
                    par défaut est 'Date_format_datetime'.
            voisinage (str, optional): durée du voisinage équivalent au rayon de la fenêtre de moyenne glissante,
                    par défaut est "1j" (1 jour).
            strptime (str, optional): format de conversion de la chaîne de caractères de voisinage en
                    pour une conversion strptime,
                    par défaut est "%dj".
        
        Retourne :
        ------------
            retourne une table de moyenne glissante dont le rayon de la fenêtre correspond à une durée
            
        Exemples :
        ------------
        >>> salaire = Table(['Name', 'Date', 'Salary', 'Date_format_datetime'], [['Alice', '21/04/2021', 122000, datetime.datetime(2021, 4, 21, 0, 0)], ['Bob', '22/04/2021', 77000.4, datetime.datetime(2021, 4, 22, 0, 0)], ['Anne', '24/04/2021', 119000.5, datetime.datetime(2021, 4, 24, 0, 0)], ['Steve', '21/04/2021', 12000, datetime.datetime(2021, 4, 21, 0, 0)], ['Gabrielle', '18/04/2021', 68000, datetime.datetime(2021, 4, 18, 0, 0)], ['Jehanne', '25/04/2021', 229000, datetime.datetime(2021, 4, 25, 0, 0)], ['Donatien', '19/04/2021', np.nan, datetime.datetime(2021, 4, 19, 0, 0)]])
        >>> moyenne_glissante = MoyenneGlissante(variable = 'Salary', voisinage = "2j", strptime = "%dj")
        >>> salaire_glissant = moyenne_glissante.appliquer(salaire)
        >>> print(salaire_glissant)
        ['Name', 'Date', 'Salary', 'Date_format_datetime']
        **************************************************
        ['Alice', '21/04/2021', 70333.46666666666, datetime.datetime(2021, 4, 21, 0, 0)]
        ['Bob', '22/04/2021', 69583.59166666666, datetime.datetime(2021, 4, 22, 0, 0)]
        ['Steve', '21/04/2021', 50639.01944444444, datetime.datetime(2021, 4, 21, 0, 0)]
        """
        self.variable = variable
        self.date = date
        self.strptime = strptime
        # Conversion de la durée str en durée timedelta
        # "14j 9h 8m 34s" devient la durée de 14 jours 9 heures 8 minues et 34 secondes
        # requierant le strptime "%dj %Hh %Mm %Ss" / NE PAS INDIQUER ANNEE et MOIS
        date_time = datetime.datetime.strptime(voisinage, strptime)
        # day, hour, minute, second, microsecond
        self.voisinage = datetime.timedelta(days=date_time.day, hours=date_time.hour,
            minutes=date_time.minute, seconds=date_time.second, microseconds=date_time.microsecond)

    def appliquer(self, table:Table):
        # /!\ Utilisation de la fonction moyenne crée plus tard (qui calcule la moyenne meme s'il y a des valeurs manquantes)
        # récupération de l'indice de la variable choisies sur lesquelles appliquer la moyenne
        indice_variable = table.header.index(self.variable)
        indice_date = table.header.index(self.date)
        # DEEPCOPY est essentiel, car on reprendra une sous-liste de body et remplacerons chaque valeurs de variable par leur moyenne glissante
        # sans DEEPCOPY, remplacer les valeurs de la colonne variable du tableau body modifiera le tableau table
        # création de body vide
        header, body = table.header, deepcopy(table.body)
        longueur_body = len(body)
        new_body = []
        # recherche de l'intervalle de la date du tableau
        dates = [body[ligne][indice_date] for ligne in range(longueur_body)]
        date_min, date_max = min(dates), max(dates)
        voisinage = self.voisinage
        for ligne in body :
            # La fenêtre de centre de date body[indice_date] et de rayon voisinage est-elle contenue dans l'intervalle des dates ?
            date = ligne[indice_date]
            if date >= date_min + voisinage and date <= date_max - voisinage :
                # On recopie la ligne
                new_body.append(ligne)
                # On va remplacer la colonne de la variable par sa moyenne glissante ie une moyenne sur la fenêtre suivante
                # Création de la fenêtre
                fenetre = []
                for ligne_glissante in body :
                    # valeurs contenues dans la fenêtre de calcul de moyenne
                    # l'intervalle de la fenêtre de calcul au point de date date est [voisin_min, voisin_max] tel que
                    voisin_min = date - voisinage
                    voisin_max = date + voisinage
                    if ligne_glissante[indice_date] >= voisin_min and ligne_glissante[indice_date] <= voisin_max :
                        fenetre.append(ligne_glissante[indice_variable])
                # On peut désormer remplacer la valeur de la variable dans le body (ou plutôt sa copie : new_body)
                # par sa moyenne glissante, ie la moyenne de la fenêtre calculée
                new_body[-1][indice_variable] = MoyenneGlissante("variable", "date").moyenne(fenetre)
        return Table(header, new_body)

    def moyenne(self, liste:list):
        """Calcule la moyenne d'une liste en outrepassant les valeurs manquantes np.nan

        Paramètres :
            liste (list) : liste d'éléments float ou int (np.nan est de type float)

        Retourne :
            float : retourne la moyenne de la liste
        
        Exemple :
        >>> MoyenneGlissante("variable", "date").moyenne([0, 2, np.nan])
        1.0
        >>> MoyenneGlissante("variable", "date").moyenne([np.nan]*4)
        nan
        """
        # Somme des indicatrices (valeur est non manquante) dans liste
        vecteur_indicatrices = [1 - np.isnan(element) for element in liste]
        S_0 = sum(vecteur_indicatrices)
        if vecteur_indicatrices == [0]*len(liste) :
            # S'il n'y a que des valeurs manquantes : retourner valeur manquante
            return np.nan
        # Somme des éléments non manquants dans liste
        S_1 = np.nansum([element for element in liste])
        return S_1/S_0

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)