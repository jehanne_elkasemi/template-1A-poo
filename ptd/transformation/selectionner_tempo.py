import datetime as d
from ptd.transformation import Transformation
from ptd.modeles.table import Table

class SelectionnerTempo(Transformation):
    '''Créer un objet permettant de selctionner une plage temporelle d'une table (qui doit avoir une colonne 'Date_format_datetime', si ce n'est pas le cas, utilisez FormaterDate)

    Paramètres
    ----------
   date_debut : str
   date_fin : str
   format : str (le format datetime des dates données)

    Retourne
    -------
    None

    Exemples
    --------
    >>>

    '''
    def __init__(self, date_debut : str, date_fin : str, format : str):
        self.date_debut=date_debut
        self.date_fin=date_fin
        self.format=format

    def appliquer(self, table):
        '''Permet d'appliquer un objet permettant de formater les dates de la table au format datetime

        Paramètres
        ----------
        table : Table

        Retourne
        -------
        table: Table

        la table de la plage selectionnée

        Exemples
        --------
        >>> table=Table(['date','Date_format_datetime'],[['03/07/2001',d.datetime(2001, 7, 3, 0, 0)],['05/01/2001',d.datetime(2001, 1, 5, 0, 0)],['02/07/1969',d.datetime(1969, 7, 2, 0, 0)],['01/05/1968',d.datetime(1968, 5, 1, 0, 0)]])
        >>> selection=SelectionnerTempo('02/07/1969','05/06/2001', '%d/%m/%Y')
        >>> table_plage=selection.appliquer(table)
        >>> table_plage.body
        [['05/01/2001', datetime.datetime(2001, 1, 5, 0, 0)], ['02/07/1969', datetime.datetime(1969, 7, 2, 0, 0)]]

        '''
        body=table.body
        date_debut=d.datetime.strptime(self.date_debut, self.format)
        date_fin=d.datetime.strptime(self.date_fin, self.format)
        indice_date=table.header.index("Date_format_datetime")
        selection=[]
        for individu in body : 
            #on garde les lignes de la table dont la date et dans la plage voulue
            if (individu[indice_date]>=date_debut) and (individu[indice_date]<=date_fin) : 
                selection.append(individu)
        return Table(table.header, selection)

if __name__ == '__main__':
    import doctest
    doctest.testmod()