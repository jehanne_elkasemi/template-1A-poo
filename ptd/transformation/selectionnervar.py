from copy import copy
from ptd.transformation.transformation import Transformation
from copy import copy, deepcopy 
from ptd.modeles.table import Table

class SelectionnerVar(Transformation):
    """ Selectionne une variable de la table
    Attributes
    -----------
    nom_variable : str
            le nom de la variable à selectionner
    Examples
    ---------
    >>> table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
    >>> var_selection = SelectionnerVar("id")
    """
    def __init__(self, liste_variable):
        """ Constructeur 
        Parameters
        ---------
        liste_variable : str list
            la liste contenant le nom de la variable à selectionner
        """
        self.liste_variable = liste_variable
    def appliquer(self,table):
        """ Applique la selection de variable sur une table

        Parameters
        ---------
        table : Table
            table dont on veut selectionner la variable
        Examples
        ---------
        >>> table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
        >>> var_selection = SelectionnerVar("id")
        >>> var_selection.appliquer(table)
        [["id"], [["ARA"], ["BFC"], ["BRE"], ["CVL"]]]
        """
        table_sortie = Table(deepcopy(table.header), deepcopy(table.body))
        for k in table.header:
            if k not in self.liste_variable:
                table_sortie.supprimer_colonne(k)
        return(table_sortie)
       

