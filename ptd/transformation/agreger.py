from ptd.modeles.table import Table
from ptd.transformation.transformation import Transformation
import datetime as d

class Agreger(Transformation):
    '''Créer un objet permettant d'agreger l'échelle régionale en échelle nationale d'une table en ne faisant les moyennes des var numérique demandées

    Paramètres
    ----------
    variables_conservees : str list (liste de noms des variables numériques conservées pendant l'agregation)

    Retourne
    -------
    None

    Exemples
    --------
    >>>

    '''
    def __init__(self,variables_conservees):
        self.variables_conservees=variables_conservees

    def appliquer(self, table):
        '''Permet d'appliquer l'agragation d'un objet agregation sur une table

        Paramètres
        ----------
        table : Table

        Retourne
        -------
        Table
        La table avec l'échelle agregée

        Exemples
        --------
        >>> table=Table(['Date_format_datetime','région','variable numérique'], [[d.datetime(2022, 5, 23, 15, 7, 40, 135998),'Nord',59],[d.datetime(2022, 5, 23, 15, 7, 40, 135998),'Pas-de-Calais',62],[d.datetime(2023, 5, 23, 15, 7, 40, 135998),'Nord',59]])
        >>> agreger=Agreger(['variable numérique'])
        >>> table_agregee=agreger.appliquer(table)
        >>> print(table_agregee)
        ['Date_format_datetime', 'variable numérique']
        **********************************************
        [datetime.datetime(2022, 5, 23, 15, 7, 40, 135998), 60.5]
        [datetime.datetime(2023, 5, 23, 15, 7, 40, 135998), 59.0]
        '''
        body=table.body
        noms_vars=self.variables_conservees
        indice_date=table.header.index('Date_format_datetime')
        #on recupere les indices des var a conserver
        indices_vars=[indice_date]
        for name in noms_vars :
            indices_vars.append(table.header.index(name))
        #on crée le nouveau_header
        nouveau_header=[]
        for i in indices_vars : 
            nouveau_header.append(table.header[i])
        #on recupere les dates de la table
        dates=[]
        for individu in body : 
            if individu[indice_date] not in dates:
                dates.append(individu[indice_date])
        #on crée un nouveau body avec pour l'instant seulement les dates
        nouveau_body=[[date] for date in dates]
        #on fait la moyenne de de chaque var pour chaque date
        for var in indices_vars[1:]:
            for date in dates :
                sum=0
                compteur=0
                indice_nouveau_individu=dates.index(date)
                for ancien_individu in body :
                    if ancien_individu[indice_date]==date:
                        sum+=ancien_individu[var]
                        compteur+=1
                nouveau_body[indice_nouveau_individu].append(sum/compteur)
        return Table(nouveau_header,nouveau_body)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
  

