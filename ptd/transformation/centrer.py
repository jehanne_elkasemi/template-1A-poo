from ptd.transformation.transformation import Transformation
from ptd.modeles.table import Table

class Centrer(Transformation):
    """ Centrer une variable de la table

    Attributes
    -----------
    nom_variable : str
            le nom de la variable à standardiser
    nb_chiffre_virgule : int, default 2
        nombre de chiffre a garder apres la virgule
    Examples
    ---------
    >>> table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
    >>> var_centre = Centrer("nb_habitant")
    """
    def __init__(self,nom_variable, nb_chiffre_virgule = 2):
        """ Constructeur 

        Parameters
        ---------
        nom_variable : str
            le nom de la variable à centrer

        nb_chiffre_virgule : int, default 2
            nombre de chiffre a garder apres la virgule
            par defaut 2
        """
        self.nom_variable = nom_variable
        self.nb_chiffre_virgule = nb_chiffre_virgule
    def appliquer(self,table):
        """ Centre la variable et l'applique a une table

        Parameters
        ---------
        table : Table
            table dont on veut centrer la variable
            la variable doit etre numerique

        Returns 
        -------
        Table
            table avec la variable centree

        Examples
        ---------
        >>> table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
        >>> var_centre = Centrer("nb_habitant")
        >>> var_centre.appliquer(table)
        [["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 3848799.5 ], ["BFC", "Bourgogne-Franche-Comté", -1388557.5 ], ["BRE", "Bretagne", -839283.5], ["CVL", "Centre-Val de Loire", 2573180, -1620957.5]]]
        """
        sum = 0
        moyenne = 0
        table_sortie = Table(table.header, table.body)
        index_var = table.header.index(self.nom_variable)
        for k in range(len(table.body)):
            sum += table.body[k][index_var]
        moyenne = sum / len(table.body)
        for k in range(len(table_sortie.body)):
            table_sortie.body[k][index_var] -= moyenne
            table_sortie.body[k][index_var] = round(table_sortie.body[k][index_var], self.nb_chiffre_virgule)
        return(table_sortie)
