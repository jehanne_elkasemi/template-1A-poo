import numpy as np
from ptd.modeles.table import Table
from ptd.transformation.transformation import Transformation
import copy

class RemplacerValManquante(Transformation):
    '''Créer un objet permettant de remplacer les valeurs manquantes en caractère par des nan de numpy

    Paramètres
    ----------
    val_manquante : str (chaine de caractère des valeurs manquantes)

    Retourne
    -------
    None

    Exemples
    --------
    

    '''
    def __init__(self, val_manquante : str):
        self.val_manquante=val_manquante

    def appliquer(self, table):
        '''Permet d'appliquer un objet permettant d'agreger l'échelle régionale en échelle nationale d'une table en ne faisant les moyennes des var numérique demandées

        Paramètres
        ----------
        table : Table

        Retourne
        -------
        Table
        la table modifiée avec des np.nan à la place des valeurs manquantes

        Exemples
        --------
        >>> table = Table(['id', 'val_num', 'val_cara'], [[42, 'N/A', 'Bernard Friot'], [43,68,'Franck Lepage'], [44,'N/A','Gael Tanguy']])
        >>> remplacer_na=RemplacerValManquante('N/A')
        >>> table2=remplacer_na.appliquer(table)
        >>> print(table2)
        ['id', 'val_num', 'val_cara']
        *****************************
        [42, nan, 'Bernard Friot']
        [43, 68, 'Franck Lepage']
        [44, nan, 'Gael Tanguy']

    '''
        table_vf=Table(table.header,[])
        for individu in table.body :
            for i in range(len(individu)):
                if individu[i]==self.val_manquante :
                    individu[i]=np.nan
            table_vf.body.append(individu)
        return table_vf

if __name__ == '__main__':
    import doctest
    doctest.testmod()