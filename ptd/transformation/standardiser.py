from ptd.transformation.transformation import Transformation
import numpy as np
from ptd.modeles.table import Table
from copy import deepcopy

class Standardiser(Transformation):
    """ Standardiser une variable de la table

    Attributes
    -----------
    nom_variable : str
            le nom de la variable à standardiser
    nb_chiffre_virgule : int, default 2
            nombre de chiffre a garder apres la virgule
    Examples
    ---------
    >>> table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
    >>> var_standardise = Standardiser("nb_habitant")
    """
    def __init__(self, nom_variable, nb_chiffre_virgule = 2):
        """ Constructeur 
        Parameters
        ---------
        nom_variable : str
            le nom de la variable à standardiser
            
        nb_chiffre_virgule : int, default 2
            nombre de chiffre a garder apres la virgule
        """
        self.nom_variable = nom_variable
        self.nb_chiffre_virgule = nb_chiffre_virgule
    def appliquer(self, table): 
        """ Applique la standardisation de la variable sur une table

        Parameters
        ---------
        table : Table
            table dont on veut standardiser la variable
            la variable doit etre numerique

        Returns 
        -------
        Table
            table avec la variable standardisee

        Examples
        ---------
        >>> table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
        >>> var_standardisee = Standardiser("nb_habitant")
        >>> var_standardisee.appliquer(table)
        [["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 1.72 ], ["BFC", "Bourgogne-Franche-Comté", -0.62 ], ["BRE", "Bretagne", -0.37], ["CVL", "Centre-Val de Loire", 2573180, -0.72]]]
        """
        # assert Cleaner(table).error_var_header(self.nom_variable) == True, "la variable n'est pas numerique"

        L=[]
        table_sortie = Table()
        table_sortie.header = deepcopy(table.header)
        table_sortie.body = deepcopy(table.body)
        index_var = table.header.index(self.nom_variable)
        for k in range(len(table.body)):
            L.append(table.body[k][index_var])
        moyenne = np.mean(L)
        ecart_type = np.std(L)
        for k in range(len(table_sortie.body)):
            table_sortie.body[k][index_var] -= moyenne
            table_sortie.body[k][index_var] = table_sortie.body[k][index_var] / ecart_type
            table_sortie.body[k][index_var] = round(table_sortie.body[k][index_var], self.nb_chiffre_virgule)
        return(table_sortie)

