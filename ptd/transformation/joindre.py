from ptd.transformation.transformation import Transformation
from ptd.modeles.table import Table
import numpy as np


class Joindre(Transformation) :
    """ Joindre la table avec une autre
    Attributes
    -----------
    cle_table : str
            le nom de la variable qui correspond à la cle de jointure de la table1
    cle_table_join : str
            le nom de la variable qui correspond à cle_table dans la table2 (optionnel)
            par défaut la même que cle_table
    cle_deux_table : str
            le nom de la variable qui correspond à la deuxieme variable qui permet de constituer une clef primaire dans la table1 (optionnel)
    cle_deux_join : str
            le nom de la variable qui correspond à cle_deux_join dans la table2 (optionnel)
            par défaut la même que cle_deux_table
    table : Table
            la table à joindre     
    Examples
    ---------
    >>> table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
    >>> joindre = Joindre(table, "id")
    """
    def __init__(self,table,cle_table, cle_table_join=0, cle_deux_table=0, cle_deux_join = 0):
        """ Constructeur de la classe Joindre
        Parameters
        ---------
        cle_table : str
            le nom de la variable qui correspond à la cle de jointure de la table1
        cle_table_join : str
            le nom de la variable qui correspond à cle_table dans la table2 (optionnel)
            par défaut la même que cle_table
        cle_deux_table : str
            le nom de la variable qui correspond à la deuxieme variable qui permet de constituer une clef primaire dans la table1 (optionnel)
        cle_deux_join : str
            le nom de la variable qui correspond à cle_deux_join dans la table2 (optionnel)
            par défaut la même que cle_deux_table
        table : Table
            la table à joindre
        """ 
        self.table = table
        self.cle_table = cle_table
        self.cle_deux_table = cle_deux_table

        if cle_deux_join == 0:
            self.cle_deux_join = self.cle_deux_table
        else:
            self.cle_deux_join = cle_deux_join

        if cle_table_join == 0:
            self.cle_table_join = self.cle_table
        else:
            self.cle_table_join = cle_table_join

        

    def appliquer(self,table_join):
        """ Applique la jointure des deux tables

        Parameters
        ---------
        table : Table
            table que l'on veut joindre avec l'autre
        Returns 
        -------
        Table
            table qui correspond à la jointure des deux tables
        Examples
        ---------
        >>> table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
        >>> table2 = Table(["id", "superficie"], [["ARA", 69711], ["BFC", 47783]])
        >>> joindre = Joindre(table, "id")
        >>> print(joindre.appliquer(table2))
        ['region', 'nb_habitant', 'id', 'superficie']
        *********************************************
        ['Auvergne-Rhône-Alpes', 8042936, 'ARA', 69711]
        ['Bourgogne-Franche-Comté', 2805580, 'BFC', 47783]
        >>> table3 = Table(["id", "superficie", "date"], [["ARA", 69711, 2022], ["ARA", 69700, 2020], ["BFC", 47783, 2022], ["BFC", 47700, 2020]])
        >>> table4 = Table(["id", "region", "nb_habitant", "date"], [["ARA", "Auvergne-Rhône-Alpes", np.nan , 2020], ["ARA", "Auvergne-Rhône-Alpes", 8042936, 2017 ]])
        >>> joindre = Joindre(table3, "id", "id", "date")
        >>> print(joindre.appliquer(table4))
        ['superficie', 'id', 'region', 'nb_habitant', 'date']
        *****************************************************
        [69700, 'ARA', 'Auvergne-Rhône-Alpes', nan, 2020]
        """
        table_fusion = Table(self.table.header + table_join.header,[])
        index_cle_table = self.table.header.index(self.cle_table)
        index_cle_join = table_join.header.index(self.cle_table_join)
        if self.cle_deux_table == 0 :
            for k in range(len(self.table.body)):
                for i in range(len(table_join.body)):
                    if self.table.body[k][index_cle_table] == table_join.body[i][index_cle_join]:
                        table_fusion.ajouter_ligne(self.table.body[k] + table_join.body[i])
            table_fusion.supprimer_colonne(self.cle_table_join)
            return table_fusion
        else :
            index_cle_deux_table = self.table.header.index(self.cle_deux_table)
            index_cle_deux_join = table_join.header.index(self.cle_deux_join)
            for k in range(len(self.table.body)):
                for i in range(len(table_join.body)):
                    if self.table.body[k][index_cle_table] == table_join.body[i][index_cle_join] and self.table.body[k][index_cle_deux_table] == table_join.body[i][index_cle_deux_join]:
                        table_fusion.ajouter_ligne(self.table.body[k] + table_join.body[i])
            table_fusion.supprimer_colonne(self.cle_table_join)
            table_fusion.supprimer_colonne(self.cle_deux_table)
            return table_fusion


if __name__ == '__main__':
    import doctest
    doctest.testmod()

        