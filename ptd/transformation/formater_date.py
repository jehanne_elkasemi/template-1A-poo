import datetime as d
from ptd.transformation.transformation import Transformation
from ptd.modeles.table import Table

class FormaterDate(Transformation):
    '''Créer un objet permettant de formater les dates de la table au format datetime

    Paramètres
    ----------
   nom_colonne_date : str
   format : str (le format datetime des dates de la table)

    Retourne
    -------
    None

    Exemples
    --------
    >>>

    '''
    
    def __init__(self, nom_colonne_date, format : str):
        self.nom_colonne_date=nom_colonne_date
        self.format=format

    '''permet d'appliquer un objet qui formate les dates de la table

    Paramètres
    ----------
    table : Table

    Retourne
    -------
    la table donnée avec une colonne suplémentaire (Date_format_datetime) qui contient les dates au format datetime

    Exemples
    --------
    >>> table=Table(['date'],[['03/07/2001],['05/01/2001'],['02/07/1969'],['01/05/1968']])
    >>> formatage=FormaterDate('date', '%d/%m/%Y')
    >>> table_date=formatage.appliquer(table)
    >>> table.body
    [[datetime.datetime(2001, 7, 3, 0, 0)],[datetime.datetime(2001, 1, 5, 0, 0)],[datetime.datetime(1969, 7, 2, 0, 0)],[datetime.datetime(1968, 5, 1, 0, 0)]]

    '''
    def appliquer(self, table):
        body=table.body
        header=table.header
        nom_colonne_date=self.nom_colonne_date
        indice_date=header.index(nom_colonne_date)
        header.append('Date_format_datetime')
        #pour chaque individu on rajoute une valeur en datetime
        for individu in body : 
            date=d.datetime.strptime(individu[indice_date], self.format)
            individu.append(date)
        return Table(header,body)

if __name__ == '__main__':
    import doctest
    doctest.testmod()