import matplotlib.pyplot as plt
from ptd.transformation.transformation import Transformation
import ptd.modeles.boite_a_outil as fct
from ptd.modeles.table import Table

class Plot(Transformation):
    """ Tracer le graph d'une variable en fonction d'une autre
    Attributes
    -----------
    variable_ascisse : str
            le nom de la variable qui correspond à l'abscisse
    variable_ordonnee : str
            le nom de la variable qui correspond à l'ordonnee
    titre : str
            titre du graphique
            il existe un titre par défaut   
    Examples
    ---------
    >>> table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
    >>> table2 = Table(["id", "superficie"], [["ARA", 69711], ["BFC", 47783]])
    >>> joindre = Joindre(table, "id")
    >>> plot = Plot("nb_habitant", "superficie")
    >>> plot.appliquer(joindre)
    None
    """
    def __init__(self, variable_abscisse, variable_ordonnee, titre = 0):
        """ Constructeur de la classe Plot
        Parameters
        ---------
        variable_ascisse : str
            le nom de la variable qui correspond à l'abscisse
        variable_ordonnee : str
            le nom de la variable qui correspond à l'ordonnee
        titre : str
            titre du graphique
            il existe un titre par défaut
        """ 
        self.variable_abscisse = variable_abscisse
        self.variable_ordonnee = variable_ordonnee
        if titre == 0:
            self.titre = str(self.variable_ordonnee) + " en fonction de " + str(self.variable_abscisse)
        else:
            self.titre = titre
            
    def appliquer(self,table):
        """ Applique le plot

        Parameters
        ---------
        table : Table
            table qui contient les variables à plot
        Returns 
        -------
        Graphique 
            graphique représentant les deux variables 
        Examples
        ---------
        >>> table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
        >>> table2 = Table(["id", "superficie"], [["ARA", 69711], ["BFC", 47783]])
        >>> joindre = Joindre(table, "id")
        >>> plot = Plot("nb_habitant", "superficie")
        >>> plot.appliquer(joindre)
        None
        """
        x = []
        y = []
        table = fct.enlever_val_manq(table)
        index_abs = table.header.index(self.variable_abscisse)
        index_ord = table.header.index(self.variable_ordonnee)
        for k in range(len(table.body)):
            x.append(table.body[k][index_abs])
            y.append(table.body[k][index_ord])
        plt.scatter(x,y)
        plt.xlabel(str(self.variable_abscisse))
        plt.ylabel(str(self.variable_ordonnee))
        plt.title(self.titre)
        plt.show()

