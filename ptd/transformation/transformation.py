from abc import abstractmethod
from abc import ABC

class Transformation(ABC) :
    def __init__(self):
        pass
    @abstractmethod
    def appliquer(table):
        pass
