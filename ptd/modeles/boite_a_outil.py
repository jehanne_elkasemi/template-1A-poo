from ptd.importers.importer_csv import ImporterCsv
from ptd.modeles.pipeline import Pipeline
from ptd.transformation.formater_date import FormaterDate
from ptd.transformation.joindre import Joindre
#from ptd.transformation.plot import Plot
from ptd.modeles.table import Table
import math
from copy import deepcopy

#pour supprimer les lignes qui presentent des valeurs manqauntes pour une variable

def enlever_val_manq(table):
    '''Permet d'enlever les lignes qui contiennent des na pour une variable

    Paramètres
    ----------
    table : Table
    nom_var : str

    Retourne
    -------
    Table

    Exemples
    --------
    # >>> table=Table(['Date_format_datetime','région','variable numérique'], [[d.datetime(2022, 5, 23, 15, 7, 40, 135998),'Nord',nan],[d.datetime(2022, 5, 23, 15, 7, 40, 135998),'Pas-de-Calais',62],[d.datetime(2023, 5, 23, 15, 7, 40, 135998),'Nord',59]])
    # >>> print(enlever_val_manq(table,'var numérique'))
    # ['Date_format_datetime', 'variable numérique']
    # **********************************************
    # [d.datetime(2022, 5, 23, 15, 7, 40, 135998),'Pas-de-Calais',62]
    # [d.datetime(2023, 5, 23, 15, 7, 40, 135998),'Nord',59]
    '''
    for individu in table.body :
        if isinstance(individu, float) and np.isnan(individu) :
            del table.body[table.body.index(individu)]
    return(table)

#pour importer deux tables (avec des dates et des regions) aux format csv et les joindre et regarder une variables numérique en fonction d'une autre

#def comparer(chemin1, nom_var_1, nom_na1, sep1, region_1, format1, nom_date_1, chemin2, nom_var_2, nom_na2, sep2, nom_date_2, region_2,format2):
    '''Permet de tracer le graph de deux variables de deux tables différentes (non formatées) au format csv

    Paramètres
    ----------
    chemin1 : str (chemin de la table chez l'utilisateur)
    nom_var_1 : str (nom de la variable explicative)
    nom_na1 : str (str des valeurs manquantes)
    sep1 : str (str du separateur dans le ficher)
    region_1 : str (nom de la variable qui contient les noms de regions)
    format1 : str (format srtptime des dates)
    nom_date_1 : str (nom de la var qui contient les dates)
    chemin2 : str (chemin de la table chez l'utilisateur)
    nom_var_2 : str (nom de la variable a expliquer)
    nom_na2 : str (str des valeurs manquantes)
    sep2 : str (str du separateur dans le ficher)
    region_2 : str (nom de la variable qui contient les noms de regions)
    format2 : str (format srtptime des dates)
    nom_date_2 : str (nom de la var qui contient les dates)

    Retourne
    -------
    un graphique

    Exemples
    --------

    '''
    table_a=ImporterCsv(chemin1, nom_var_1, nom_na1, sep1).importer()
    table_b=ImporterCsv(chemin2, nom_var_2, nom_na2, sep2).importer()
    table_a_2=FormaterDate(nom_date_1, format1).appliquer(table_a)
    table_b_2=FormaterDate(nom_date_2, format2).appliquer(table_b)
    table=Joindre(table_b_2, 'Date_format_datetime','Date_format_datetime',region_1,region_2).appliquer(table_a_2)
    plot=Plot(nom_var_1, nom_var_2)
    plot.appliquer(table)


# faire une table constituer de deux tables qui ont les memes variables a la suite (ex: sur deux plages de dates différentes)

def fusionner(table1, table2):
    '''Permet de coller deux tables qui on les memes variables a la suite, utile si on a des table de differentes periodes

    Paramètres
    ----------
    table1 : Table
    table2 : Table

    Retourne
    -------
    Table
    La table fusionnée

    Exemples
    --------
    >>> table=Table(['Date_format_datetime','région','variable numérique'], [[d.datetime(2022, 5, 23, 15, 7, 40, 135998),'Nord',59],[d.datetime(2022, 5, 23, 15, 7, 40, 135998),'Pas-de-Calais',62],[d.datetime(2023, 5, 23, 15, 7, 40, 135998),'Nord',59]])
    >>> table2=table=Table(['Date_format_datetime','région','variable numérique'], [[d.datetime(2022, 6, 23, 15, 7, 40, 135998),'Nord',59],[d.datetime(2022, 6, 23, 15, 7, 40, 135998),'Pas-de-Calais',62],[d.datetime(2023, 6, 23, 15, 7, 40, 135998),'Nord',59]])
    >>> print(fusionner(table,table2)
    ['Date_format_datetime', 'variable numérique']
    **********************************************
    [d.datetime(2022, 5, 23, 15, 7, 40, 135998),'Nord',59]
    [d.datetime(2022, 5, 23, 15, 7, 40, 135998),'Pas-de-Calais',62]
    [d.datetime(2023, 5, 23, 15, 7, 40, 135998),'Nord',59]
    [d.datetime(2022, 6, 23, 15, 7, 40, 135998),'Nord',59]
    [d.datetime(2022, 6, 23, 15, 7, 40, 135998),'Pas-de-Calais',62]
    [d.datetime(2023, 6, 23, 15, 7, 40, 135998),'Nord',59]
    '''
    table=Table(deepcopy(table1.header), deepcopy(table1.body))
    for individu in table2.body:
        table.ajouter_ligne(individu)
    return table

