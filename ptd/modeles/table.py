from datetime import timedelta
from datetime import datetime

class Table :
    '''Une Table est composée d'un header et d'un body qui sont respectivement une liste de str (nom des variables) et une liste de liste de str qui représente les individus de la table

    Paramètres
    ----------
    header : str list (liste des noms des varibles en str)
    body : list list (liste de listes de meme taille que le header qui contient les caracteristiques des chaques individu)

    Retourne
    -------
    None

    Exemples
    --------
    >>>

    '''
    def __init__(self,header = [], body = []):
        self.body = body
        self.header = header
        
    def ajouter_ligne(self, ligne):
        ''' Ajoute une ligne a la table

        Parameters
        ---------
        ligne : str list
            nouvelle ligne a ajouter

        Examples
        -------
        >>> table = Table(['id', 'superficie'], [['ARA', 69711], ['BFC', 47783]])
        >>> table.ajouter_ligne(['BRE', 27208])
        >>> print(table)
        ['id', 'superficie']
        ********************
        ['ARA', 69711]
        ['BFC', 47783]
        ['BRE', 27208]
        '''
        self.body.append(ligne)

    def supprimer_ligne(self, nom_id, id):
        ''' Supprime une ligne a la table

        Parameters
        ---------
        id : str
             id de la ligne a supprimer

        Examples
        -------
        >>> table = Table(['id', 'superficie'], [['ARA', 69711], ['BFC', 47783]])
        >>> table.supprimer_ligne('id', 'BFC')
        >>> print(table)
        ['id', 'superficie']
        ********************
        ['ARA', 69711]
        '''
        index_id = self.header.index(nom_id)
        for individu in self.body:
            if individu[index_id] == id :
                del self.body[self.body.index(individu)]

    def ajouter_colonne(self,nom,colonne):
        ''' Ajoute une colonne a la table

        Parameters
        ---------
        nom : str 
            nom de la colonne a rajouter
        colonne : list
            liste des valeurs de la colonne a ajouter

        Examples
        -------
        >>> table = Table(['id', 'superficie'], [['ARA', 69711], ['BFC', 47783]])
        >>> table.ajouter_colonne('réponse universelle', [42, 42])
        >>> print(table)
        ['id', 'superficie', 'réponse universelle']
        *******************************************
        ['ARA', 69711, 42]
        ['BFC', 47783, 42]
        '''
        self.header.append(nom)
        for k in range(len(self.body)):
            self.body[k].append(colonne[k])

    def supprimer_colonne(self,nom):
        ''' Supprime une colonne a la table

        Parameters
        ---------
        nom : str 
            nom de la colonne a supprimer

        Examples
        -------
        >>> table = Table(['id', 'superficie'], [['ARA', 69711], ['BFC', 47783]])
        >>> table.supprimer_colonne('superficie')
        >>> print(table)
        ['id']
        ******
        ['ARA']
        ['BFC']
        '''
        index_col = self.header.index(nom)
        del self.header[index_col]
        for k in range(len(self.body)):
            del self.body[k][index_col]

    def as_numeric(self, nom):
        ''' Transforme une colonne de caractères en numériques

        Parameters
        ---------
        nom : str 
            nom de la colonne a modifier

        Examples
        -------
        >>> table = Table(['id', 'superficie'], [['ARA', '69711'], ['BFC', '47783']])
        >>> table.as_numeric('superficie')
        >>> print(table)
        ['id', 'superficie']
        ********************
        ['ARA', 69711.0]
        ['BFC', 47783.0]
        '''
        index_col = self.header.index(nom)
        for k in range(len(self.body)):
            self.body[k][index_col] = float(self.body[k][index_col])

    def __str__(self):
        L=str(self.header) + '\n'+'*'*len(str(self.header))+'\n'
        for x in self.body:
           L+=(str(x) + '\n')
        return L[:-1]


if __name__ == '__main__':
    import doctest
    doctest.testmod()