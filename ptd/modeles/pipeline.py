from ptd.transformation.transformation import Transformation

class Pipeline :
    """ Pipeline
    Attributes
    -----------
    transformation_list : str list
            liste des transformation à faire sur la table 
    Examples
    ---------
    >>> table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
    >>> table2 = Table(["id", "superficie"], [["ARA", 69711], ["BFC", 47783]])
    >>> Pipeline([Standardiser("nb_habitant"), SelectionnerVar(["nb_habitant", "id"]), Joindre(table2, "id")]) 
    """
    def __init__(self, transformation_list ):
        """ Constructeur 
        Parameters
        ---------
        transformation_list : str list
            liste des transformation à faire sur la table 
        """
        self.transformation_list = transformation_list
    def run(self, table):
        """ Applique les operations sur une table

        Parameters
        ---------
        table : Table
            table que l'on veut modifier
        Examples
        ---------
        >>> table = Table(["id", "region", "nb_habitant"], [["ARA", "Auvergne-Rhône-Alpes", 8042936 ], ["BFC", "Bourgogne-Franche-Comté", 2805580], ["BRE", "Bretagne", 3354854], ["CVL", "Centre-Val de Loire", 2573180]])
        >>> table2 = Table(["id", "superficie"], [["ARA", 69711], ["BFC", 47783]])
        >>> Pipeline([Standardiser("nb_habitant"), SelectionnerVar(["nb_habitant", "id"]), Joindre(table2, "id")]) 
        ['superficie', 'id', 'nb_habitant']
        [[69711, 'ARA', 1.72], [47783, 'BFC', -0.62]]
        """
        table_transi=table
        for k in self.transformation_list:
            k.appliquer(table_transi)
            table_transi=k.appliquer(table_transi)
        return(table_transi)

