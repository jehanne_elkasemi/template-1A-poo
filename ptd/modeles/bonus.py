import json as json
import requests
from ptd.modeles.boite_a_outil import enlever_val_manq
from ptd.modeles.table import Table
from ptd.importers.importer_csv import ImporterCsv
import os
import numpy as np
from ptd.exporters.exporter_csv import ExporterCsv


def obtention_region(table, nom_var_lat, nom_var_long):
    region_adm = []
    reg=[]
    index_var_lat = table.header.index(nom_var_lat)
    index_var_long = table.header.index(nom_var_long)
    table_enregistrement = ImporterCsv(str(os.getcwd()) + "/data/table_enregistrement.csv" , ['Latitude', 'Longitude']).importer()
    for k in range(len(table.body)):
        for i in range(len(table_enregistrement.body)):
            if table.body[k][index_var_lat] == table_enregistrement.body[i][0] and table.body[k][index_var_long] == table_enregistrement.body[i][1]:
                region_adm.append(table_enregistrement.body[i][2])
                break
        else:
            req = requests.get("https://nominatim.openstreetmap.org/reverse?lat="+str(table.body[k][index_var_lat])+"&lon="+str(table.body[k][index_var_long])+"&format=json",verify = False)
            req2 = json.loads(req.content[:999])
            if 'state' in req2['address']:
                region_adm.append(req2['address']['state'])
                table_enregistrement.ajouter_ligne([table.body[k][index_var_lat], table.body[k][index_var_long], req2['address']['state']])
            else :
                region_adm.append(np.nan)
                table_enregistrement.ajouter_ligne([table.body[k][index_var_lat], table.body[k][index_var_long], np.nan ])
    table.ajouter_colonne("region_administrative", region_adm)
    export = ExporterCsv(str(os.getcwd()) + "/data/" , "table_enregistrement", ";")
    print(str(os.getcwd()))
    export.exporter(table_enregistrement)
    return table