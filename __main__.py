from ptd.transformation.moyenne_glissante import MoyenneGlissante
from ptd.transformation.standardiser import Standardiser
from ptd.transformation.joindre import Joindre
from ptd.transformation.plot import Plot
from ptd.modeles.pipeline import Pipeline
from ptd.importers.importer_csv import ImporterCsv
from ptd.exporters.exporter_csv import ExporterCsv
from ptd.transformation.formater_date import FormaterDate
from ptd.modeles.boite_a_outil import fusionner
from ptd.transformation.agreger import Agreger
import os

path = str(os.getcwd())

### Importer les tables meteo et conso de bretagne et d aquitaine
conso_bre = ImporterCsv(path + '\\data\\Tables de données exemple\\conso_bretagne.csv',
                        [' consommation électrique', ' consommation gaz']).importer()
meteo_bre = ImporterCsv(path + '\\data\\Tables de données exemple\\meteo_bretagne.csv',
                        [' precipitations', ' force du vent', ' temperature']).importer()
conso_aquitaine = ImporterCsv(path + '\\data\\Tables de données exemple\\conso_aquitaine.csv',
                        [' consommation électrique', ' consommation gaz']).importer()
meteo_aquitaine = ImporterCsv(path + '\\data\\Tables de données exemple\\meteo_aquitaine.csv',
                        [' precipitations', ' force du vent', ' temperature']).importer()
print(conso_bre)

"""### A) Sans pipeline
# Jointure des tables
bretagne = Joindre(meteo_bre, 'date', 'date', ' régions', ' régions').appliquer(conso_bre)
aquitaine = Joindre(meteo_aquitaine, 'date', 'date', ' régions', ' régions').appliquer(conso_aquitaine)
print(bretagne)

# Plot de la conso electrique en fonction de la temperature
plot_elec_temperature = Plot(' temperature', ' consommation électrique', 'Consommation électrique en fonction de la température (Bretagne)').appliquer(bretagne)
plot_gaz_temperature = Plot(' temperature', ' consommation gaz', 'Consommation de gaz en fonction de la température (Bretagne)').appliquer(bretagne)

# Formatage date avant d'effectuer une moyenne glissante
bretagne = FormaterDate('date', '%d/%m/%Y').appliquer(bretagne)
aquitaine = FormaterDate('date', '%d/%m/%Y').appliquer(aquitaine)
print(bretagne)

# Plot conso en fonction de la date
plot_elec_date = Plot('Date_format_datetime', ' consommation électrique', 'Consommation électrique au cours du temps (Bretagne)').appliquer(bretagne)
plot_elec_temperature = Plot('Date_format_datetime', ' temperature', 'Température au cours du temps (Bretagne)').appliquer(bretagne)

# (Annexe) moyenne glissante sur 14 jours de la consommation electrique
bretagne_glissante = MoyenneGlissante(' consommation électrique', 'Date_format_datetime', "7", "%d").appliquer(bretagne)
plot_elecglissee_date = Plot('Date_format_datetime', ' consommation électrique', 'Consommation électrique lissée sur 15 jours au cours du temps (Bretagne)').appliquer(bretagne_glissante)

# Concaténation de la bretagne et de l'aquitaine
france = fusionner(bretagne, aquitaine)
print(france)

# Standardisation de la force du vent
print(france.header)
print(france.body[0])
france = Standardiser(' force du vent').appliquer(france)
print(france.body[0])

# Agrégation
france_agregee = Agreger([' consommation électrique', ' temperature', ' consommation gaz', ' precipitations', ' force du vent']).appliquer(france)
print(france_agregee)
print(len(france_agregee.body))
Plot('Date_format_datetime', ' consommation électrique', 'Consommation électrique journalière agrégée').appliquer(france_agregee)

### B) Avec un pipeline, c'est mieux
# concaténation des données météo (ainsi que des données energie) de bretagne et aquitaine avant de faire une jointure via pipeline
# Nous avons bien veillé à ce que les tables ont le même nombre de lignes et que chaque lignes correspondent en date de la la meteo à conso
meteo = fusionner(meteo_bre, meteo_aquitaine)
conso = fusionner(conso_bre, conso_aquitaine)

# pipeline
instructions = [Joindre(meteo, 'date', 'date', ' régions', ' régions'),
                FormaterDate('date', '%d/%m/%Y'),
                Agreger([' consommation électrique', ' temperature', ' consommation gaz', ' precipitations', ' force du vent']),
                Plot('Date_format_datetime', ' consommation électrique', 'Consommation électrique journalière agrégée 2')]
Pipeline(instructions).run(conso)

### Exportation
ExporterCsv(path + '\\data\\resultat\\', 'conso_meteo_bretagne_et_aquitaine_journalière').exporter(france_agregee)
"""